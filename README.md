**Reproduire une maquette en wireframe et la convertir en HTML et CSS**

Vous allez reproduire une maquette d'une page de contact en wireframe, que vous devrez ensuite développer en HTML et CSS en vous appuyant sur les spécifications fournies.

**Référentiels**

Développeur web et web mobile

**Ressource(s)**
[fontawesome](https://fontawesome.com)
[maquette a reproduire](https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/jpg/fcfcdcc5-f5d5-4ca4-a646-0c75337cbdf8.jpg)

**Contexte du projet**

Le propriétaire de la société Lander App souhaite mettre à jour sa page de contact. Il vous fournit la maquette de la dite page que vous pouvez récupérer des pièces jointes et vous demande de :

    Reproduire la maquette en wireframe avec l'outil de votre choix.
    Creer le wireframe version mobile
    Analyser la maquette et distinguer ses principaux blocs.
    Traduire la maquette en code HTML et CSS en respectant les consignes suivantes :
    Utiliser les balises sémantiques de l'HTML5 (header, main, footer, nav...);
    Implémenter la bibliothèque Font Awesome pour l'incorporation des différentes icones des réseaux sociaux;
    Appliquer un effet de survol sur les boutons;

**Modalités pédagogiques**

Le travail est à faire individuellement. Il doit être déposé sur simplonline avant minuit.

**Critères de performance**

Les wireframes réalisé doit correspondre à la maquette initiale.
Le code HTML et CSS doit être soigné (indentation, noms de classes et d'id...) et doit être identique à la maquette.

**Modalités d'évaluation**

Evaluation avec le formateur

**Livrables**

- Le lien vers votre repo gitlab contenant : les wireframes, le code HTML (contact.html) et CSS (style.css)
